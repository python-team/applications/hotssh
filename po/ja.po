# hotssh ja.po.
# Copyright (C) 2008 Free Software Foundation, Inc.
# This file is distributed under the same license as the hotssh package.
# Takeshi AIHANA <takeshi.aihana@gmail.com>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: hotssh trunk\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2008-10-05 22:26+0900\n"
"PO-Revision-Date: 2008-10-05 22:26+0900\n"
"Last-Translator: Takeshi AIHANA <takeshi.aihana@gmail.com>\n"
"Language-Team: Japanese <takeshi.aihana@gmail.com>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../hotssh.desktop.in.h:1
msgid "Connect to a remote computer using Secure Shell"
msgstr "SSH を使ってリモートのコンピュータに接続します"

#: ../hotssh.desktop.in.h:2
msgid "Secure Shell"
msgstr "セキュア・シェル"

#: ../hotssh/hotlib/timesince.py:48
msgid "year"
msgid_plural "years"
msgstr[0] "年"
msgstr[1] "年"

#: ../hotssh/hotlib/timesince.py:49
msgid "month"
msgid_plural "months"
msgstr[0] "ヶ月"
msgstr[1] "ヶ月"

#: ../hotssh/hotlib/timesince.py:50
msgid "week"
msgid_plural "weeks"
msgstr[0] "週間"
msgstr[1] "週間"

#: ../hotssh/hotlib/timesince.py:51
msgid "day"
msgid_plural "days"
msgstr[0] "日"
msgstr[1] "日"

#: ../hotssh/hotlib/timesince.py:52
msgid "hour"
msgid_plural "hours"
msgstr[0] "時間"
msgstr[1] "時間"

#: ../hotssh/hotlib/timesince.py:53
msgid "minute"
msgid_plural "minutes"
msgstr[0] "分"
msgstr[1] "分"

#. d is in the future compared to now, stop processing.
#: ../hotssh/hotlib/timesince.py:70
msgid "in the future"
msgstr "未来"

#: ../hotssh/hotlib/timesince.py:72
msgid "less than 1 minute ago"
msgstr "１分未満前"

#: ../hotssh/hotlib/timesince.py:83
#, python-format
msgid "%(number)d %(type)s, %(number2)d %(type2)s ago"
msgstr "%(number)d %(type)s、%(number2)d %(type2)s 前"

#: ../hotssh/hotlib/timesince.py:86
#, python-format
msgid "%(number)d %(type)s ago"
msgstr "%(number)d %(type)s 前"

#: ../hotssh/hotvte/vteterm.py:81 ../hotssh/hotvte/vtewindow.py:250
msgid "_Copy"
msgstr "コピー(_C)"

#: ../hotssh/hotvte/vteterm.py:81
msgid "Copy selected text"
msgstr "選択した文字列をクリップボードにコピーします"

#: ../hotssh/hotvte/vteterm.py:82 ../hotssh/hotvte/vtewindow.py:251
msgid "_Paste"
msgstr "貼り付け(_P)"

#: ../hotssh/hotvte/vteterm.py:82
msgid "Paste text"
msgstr "クリップボードから貼り付けます"

#: ../hotssh/hotvte/vteterm.py:173
msgid "Open Link"
msgstr "リンクを開く"

#: ../hotssh/hotvte/vtewindow.py:35
msgid "Tab Search"
msgstr "タブの検索"

#: ../hotssh/hotvte/vtewindow.py:242
msgid "File"
msgstr "ファイル"

#: ../hotssh/hotvte/vtewindow.py:243
msgid "_Detach Tab"
msgstr "タブを取り外す(_D)"

#: ../hotssh/hotvte/vtewindow.py:245
msgid "_Close"
msgstr "閉じる(_C)"

#: ../hotssh/hotvte/vtewindow.py:247
msgid "_Quit"
msgstr "終了(_Q)"

#: ../hotssh/hotvte/vtewindow.py:249
msgid "Edit"
msgstr "編集"

#: ../hotssh/hotvte/vtewindow.py:252
msgid "View"
msgstr "表示"

#: ../hotssh/hotvte/vtewindow.py:253
msgid "Help"
msgstr "ヘルプ"

#: ../hotssh/hotvte/vtewindow.py:254
msgid "_About"
msgstr "情報(_A)"

#: ../hotssh/sshwindow.py:97
msgid ""
"An interface to OpenSSH, a Secure Shell client\n"
"\n"
msgstr ""
"セキュア・シェルのクライアントである OpenSSH の GUI 版です。\n"
"\n"

#: ../hotssh/sshwindow.py:335
msgid "Options"
msgstr "オプション"

#: ../hotssh/sshwindow.py:346
msgid "Example: "
msgstr "例: "

#: ../hotssh/sshwindow.py:361
msgid "New Secure Shell Connection"
msgstr "新しいセキュア・シェルの接続"

#: ../hotssh/sshwindow.py:374
msgid "C_onnect"
msgstr "接続する(_O)"

#: ../hotssh/sshwindow.py:401
msgid "Host: "
msgstr "ホスト名: "

#: ../hotssh/sshwindow.py:420
msgid "User: "
msgstr "ユーザ名: "

#: ../hotssh/sshwindow.py:444
msgid "History"
msgstr "接続の履歴"

#: ../hotssh/sshwindow.py:456 ../hotssh/sshwindow.py:1424
msgid "Connection"
msgstr "接続先"

#: ../hotssh/sshwindow.py:459
msgid "Time"
msgstr "時間"

#: ../hotssh/sshwindow.py:465
msgid "Local"
msgstr "ローカル"

#: ../hotssh/sshwindow.py:473
msgid "Name"
msgstr "名前"

#: ../hotssh/sshwindow.py:476
msgid "Address"
msgstr "IP-アドレス"

#: ../hotssh/sshwindow.py:1137
msgid "Connection closed"
msgstr "接続を閉じました"

#: ../hotssh/sshwindow.py:1303
msgid "Connecting"
msgstr "接続中です"

#: ../hotssh/sshwindow.py:1305
#, python-format
msgid "Connected; %.2fs latency; %s"
msgstr "接続中; %.2f秒遅れ; %s"

#: ../hotssh/sshwindow.py:1307
msgid "Connection timed out"
msgstr "接続がタイムアウトしました"

#: ../hotssh/sshwindow.py:1309
msgid "Checking connection"
msgstr "接続の確認中"

#: ../hotssh/sshwindow.py:1334
msgid "Network connection changed"
msgstr "ネットワークの接続が変更になりました"

#: ../hotssh/sshwindow.py:1418
msgid "Connect to server"
msgstr "サーバへ接続"

#: ../hotssh/sshwindow.py:1419
msgid "Open a new Secure Shell connection"
msgstr "新しいセキュア・シェルの接続を開きます"

#: ../hotssh/sshwindow.py:1420
msgid "New tab for connection"
msgstr "新たにタブで開く"

#: ../hotssh/sshwindow.py:1421
msgid "Open a new tab for the same remote computer"
msgstr "現在のリモート接続に対して新しいタブを開きます"

#: ../hotssh/sshwindow.py:1422
msgid "Open SFTP"
msgstr "SFTP で開く"

#: ../hotssh/sshwindow.py:1423
msgid "Open a SFTP connection"
msgstr "SFTP の接続を開きます"

#: ../hotssh/sshwindow.py:1425 ../hotssh/sshwindow.py:1515
msgid "_Reconnect"
msgstr "再接続(_R)"

#: ../hotssh/sshwindow.py:1425
msgid "Reset connection to server"
msgstr "サーバへの接続をリセットします"

#: ../hotssh/sshwindow.py:1426
msgid "R_econnect All"
msgstr "全て再接続(_E)"

#: ../hotssh/sshwindow.py:1426
msgid "Reset all connections"
msgstr "全ての接続をリセットします"

#: ../hotssh/sshwindow.py:1514
msgid "Restore saved session?"
msgstr "保存したセッションをリストアしますか？"

#: ../hotssh/sshwindow.py:1519
#, python-format
msgid "Reconnect to %d host"
msgid_plural "Reconnect to %d hosts"
msgstr[0] " %d個のホストに再接続します"
msgstr[1] " %d個のホストに再接続します"
